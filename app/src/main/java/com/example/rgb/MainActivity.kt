package com.example.rgb

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {

    private lateinit var tvRed: TextView
    private lateinit var tvGreen: TextView
    private lateinit var tvBlue: TextView

    private lateinit var receiver: BroadcastReceiver
    private lateinit var filter: IntentFilter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        this.tvRed = findViewById(R.id.tvRed)
        this.tvGreen = findViewById(R.id.tvGreen)
        this.tvBlue = findViewById(R.id.tvBlue)

        this.receiver = Unblocked()
        this.filter = IntentFilter()
        this.filter.addAction(Intent.ACTION_USER_PRESENT)

        when {
            intent?.action == Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    this.updateText(intent)
                }
            } else -> {
                this.resetText()
            }
        }
    }

    private fun updateText(intent: Intent) {
        this.tvRed.text = intent.getStringExtra(Intent.EXTRA_TEXT)?.toString()
        this.tvGreen.text = intent.getStringExtra(Intent.EXTRA_TEXT)?.toString()
        this.tvBlue.text = intent.getStringExtra(Intent.EXTRA_TEXT)?.toString()
    }

    private fun resetText() {
        this.tvRed.text = "RED"
        this.tvGreen.text = "GREEN"
        this.tvBlue.text = "BLUE"
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(this.receiver, this.filter)
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(this.receiver)
    }

    inner class Unblocked: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            this@MainActivity.resetText()
        }
    }
}
